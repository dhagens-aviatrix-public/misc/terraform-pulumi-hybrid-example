# terraform-pulumi-hybrid-example

This demonstrates an example of how to combine Pulumi and Terraform, in case there is no Pulumi support for one of the vendors.

1. Deploy the pulumi stack in pulumi-project folder
    This will create a VPC, with subnets and route tables.
    Additionally you can add any other resources you would like to deploy via Pulumi (e.g. the application stack)

2. Deploy the Terraform code in the terraform-project folder
    This will deploy the Aviatrix transit vpc and gateway. It will also deploy a spoke gateway in the VPC created through Pulumi and attach it to the transit.