"""An AWS Python Pulumi program"""

import pulumi
import pulumi_aws as aws
from netaddr import *

cidr = '10.1.0.0/16'
name = 'pulumitest'

# Some ip subnetting logic
vpc_cidr = IPNetwork(cidr)
masklength = int(str(vpc_cidr.cidr).split("/")[1])
newbits = 8
subnet_cidrs = list(vpc_cidr.subnet(masklength+newbits))

vpc = aws.ec2.Vpc(name,
                  cidr_block=str(vpc_cidr.cidr),
                  tags={
                      "Name": name,
                  })

subnet_dict = {
    "private1": {
        "name": "Private1",
        "cidr": str(subnet_cidrs[1]),
        "rtb": "private1"
    },
    "private2": {
        "name": "Private2",
        "cidr": str(subnet_cidrs[2]),
        "rtb": "private2"
    },
    "public1": {
        "name": "Public1",
        "cidr": str(subnet_cidrs[3]),
        "rtb": "public1"
    },
    "public2": {
        "name": "Public2",
        "cidr": str(subnet_cidrs[4]),
        "rtb": "public2"
    },
    "gateway1": {
        "name": "gateway1",
        "cidr": str(subnet_cidrs[5]),
        "rtb": "gateway"
    },
    "gateway2": {
        "name": "gateway2",
        "cidr": str(subnet_cidrs[6]),
        "rtb": "gateway"
    },
}

subnets = {}
for k, v in subnet_dict.items():
    subnets[k] = aws.ec2.Subnet(k,
                                vpc_id=vpc.id,
                                cidr_block=v["cidr"],
                                tags={
                                    "Name": v["name"],
                                })

route_table_list = []
for k, v in subnet_dict.items():
    route_table_list.append(v['rtb'])
route_table_list = set(route_table_list)  # remove redundant items

route_tables = {}
for item in route_table_list:
    route_tables[item] = aws.ec2.RouteTable(item,
                                            vpc_id=vpc.id,
                                            tags={
                                                "Name": item,
                                            })
route_table_associations = {}
for k, v in subnet_dict.items():
    route_table_associations[item] = aws.ec2.RouteTableAssociation(k,
                                                                   subnet_id=subnets[k].id,
                                                                   route_table_id=route_tables[v['rtb']].id,
                                                                   )

gw = aws.ec2.InternetGateway("gw",
                             vpc_id=vpc.id,
                             tags={
                                 "Name": "main",
                             })

route = aws.ec2.Route("default_route",
                      route_table_id=route_tables["gateway"].id,
                      destination_cidr_block="0.0.0.0/0",
                      gateway_id=gw.id,
                      )


pulumi.export('vpc', vpc.id)
pulumi.export('gw_subnet_cidr', subnets['gateway1'].cidr_block)
pulumi.export('hagw_subnet_cidr', subnets['gateway2'].cidr_block)
