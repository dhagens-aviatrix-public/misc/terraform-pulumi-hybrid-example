provider "aviatrix" {
  controller_ip           = var.aviatrix_controller_ip
  username                = "admin"
  password                = var.aviatrix_admin_password
  skip_version_validation = true
}

provider "pulumi" {
  # Configuration options
  token = var.pulumi_token
}