module "mc_transit_aws" {
  source  = "terraform-aviatrix-modules/mc-transit/aviatrix"
  version = "v2.1.5"

  cloud   = "AWS"
  cidr    = "10.4.0.0/23"
  region  = "us-east-1"
  account = "AWS"
  ha_gw   = false
}

data "pulumi_stack_outputs" "pulumi_project" {
  organization = "dhagens"
  project      = "pulumi-project"
  stack        = "dev"
}

module "spoke_aws_1" {
  source  = "terraform-aviatrix-modules/mc-spoke/aviatrix"
  version = "1.2.4"

  cloud      = "AWS"
  name       = "App1"
  region     = "us-east-1"
  account    = "AWS"
  ha_gw      = false
  transit_gw = module.mc_transit_aws.transit_gateway.gw_name

  use_existing_vpc = true
  vpc_id           = data.pulumi_stack_outputs.pulumi_project.stack_outputs.vpc
  gw_subnet        = data.pulumi_stack_outputs.pulumi_project.stack_outputs.gw_subnet_cidr
  hagw_subnet      = data.pulumi_stack_outputs.pulumi_project.stack_outputs.hagw_subnet_cidr
}
