terraform {
  required_providers {
    aviatrix = {
      source  = "aviatrixsystems/aviatrix"
      version = "~>2.22.0"
    }
    pulumi = {
      source  = "transcend-io/pulumi"
      version = "0.0.2"
    }
  }
  required_version = ">= 1.0.0"
}
